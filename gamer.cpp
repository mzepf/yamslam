#include "gamer.h"
#include <iostream>

using namespace std;

void Gamer::getUserMove() {
	//have something here for the option to take highest coin
	int input = -2;
	int counter = 0;
	cout << "Enter a dice you would like to roll, if you dont want to roll a die enter -1" << endl;
	cin >> input;
	while (input != -1 && counter < 6) {
		move[counter] = input;
		counter++;
		cout << "Enter a dice you would like to roll, if you dont want to roll a die enter -1" << endl;
		cin >> input;
	}
	applyUserMove(move);
}

void Gamer::applyUserMove(int(move)[]) {
	for (int i = 0; i < 5; i++) {
		board.getDiceatIndex(move[i]).rollDie();
	}
}



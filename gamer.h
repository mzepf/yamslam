#ifndef _GAMER_H_
#define _GAMER_H_
#include "Boardr.h"

class Gamer {
	private:
		
		int move[5];
	public:
		//gets user move and puts it into a array and returns it
		void getUserMove();

		//applies user move to
		void applyUserMove(int (move)[]);

		Board board;



};

#endif // ! _GAMER_H_

